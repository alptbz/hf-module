import json
from datetime import datetime
from xhtml2pdf import pisa
import pyphen


def hyphenate_and_wrap(text, width):
    dic = pyphen.Pyphen(lang='de')

    def split_word(word):
        # Only split words longer than the width
        if len(word) > width:
            # Hyphenate the word using Pyphen
            hyphenated = dic.inserted(word, hyphen='-')
            parts = hyphenated.split('-')

            current_part = ""
            remaining_part = ""

            # Collect segments until the first part is within the width
            for part in parts:
                if len(current_part) + len(part) + 1 <= width:
                    current_part += part
                else:
                    remaining_part += part


            # Rebuild the word with a hyphen
            if remaining_part:
                return current_part + '-\n' + remaining_part
            else:
                return word
        else:
            return word

    # Process the text by splitting long words and preserving short ones
    split_text = ' '.join(split_word(word) for word in text.split())

    return split_text


def safe_to_pdf(content, filename):
    result_file = open(filename, "w+b")

    # convert HTML to PDF
    pisa_status = pisa.CreatePDF(
            content,                # the HTML to convert
            dest=result_file)           # file handle to recieve result

    # close output file
    result_file.close()                 # close output file

    # return False on success and True on errors
    return pisa_status.err


def today_str():
    return datetime.now().strftime("%d.%m.%Y")


def now_str():
    return datetime.now().strftime("%d.%m.%Y %H:%M:%S")
