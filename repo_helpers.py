import logging
import os
from git import Repo, GitCommandError

import config


def create_or_update_repos(no_update: bool = False):
    if not os.path.exists(config.WORKDIR_PATH):
        os.mkdir(config.WORKDIR_PATH)

    repos = []

    for repo_entity in config.REPOS:
        repo_url = repo_entity["url"]
        repo_name = repo_url.split('/')[-1]
        repo_path = str(os.path.join(config.WORKDIR_PATH, repo_name))
        logging.debug(f"Creating/Updating content for repo {repo_name}")
        repo: Repo = None
        if not os.path.exists(repo_path):
            repo = Repo.clone_from(repo_url, repo_path)
            logging.info(f"Cloned repo {repo_url} to {repo_path}")
        else:
            repo = Repo(repo_path)
            logging.info(f"Loaded existing repo {repo_url}")
            if not no_update:
                repo.git.reset('--hard')
                repo.remotes.origin.pull()
                logging.info(f"Pulled changes for repo {repo_url}")

        try:
            repo.remotes.origin.fetch()  # Fetch all remote branches
            remote_branches = [ref.remote_head for ref in repo.remotes.origin.refs]
            if 'sbfi' in remote_branches:
                repo.git.checkout('sbfi')
                logging.info(f"Checked out 'sbfi' branch in repo {repo_name}")
            else:
                logging.info(f"'sbfi' branch not found in repo {repo_name}, remaining on the current branch.")
        except GitCommandError as e:
            logging.error(f"Error while checking for 'sbfi' branch in repo {repo_name}: {e}")


        repos.append({"url": repo_url, "name": repo_name, "path": repo_path})

    return repos