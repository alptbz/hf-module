import re
from typing import List

class HfModul:

    def __init__(self, safe_code, kuerzel: str, name: str):
        self.safe_code: str = safe_code
        self.kuerzel: str = kuerzel
        self.name: str = name
        self.beschreibung: str = ""
        self.lernziele: str = ""
        self.modulspezifische_handlungskompetenzen: str = ""
        self.modulspezifische_handlungskompetenzen_nums = []
        self.allgemeine_handlungskompeten: str = ""
        self.allgemeine_handlungskompeten_nums = []
        self.praesenzlektionen_vor_ort: int = 0
        self.selbststudium: int = 0
        self.praesenzlektionen_distance_learning: int = 0
        self.bereich: str = ""
        self.semester: int = 0
        self.methoden: str = ""
        self.schluesselbegriffe: str = ""
        self.dispensation: str = ""
        self.lehr_und_lernformen: str = ""
        self.repository: str = ""
        self.voraussetzungen: str = ""
        self.autor: str = ""
        self.last_change: str = ""
        self.kompetenzniveaus: List[int] = []
        self.transfer: str = ""
        self.handlungskompetenzen = []
        self.kompetenzmatrix = None
        self.kompetenzmatrix_nested = None
        self.fahrplan = None
        self.handlungssituationen = None


    def get_safe_kuerzel(self):
        kuerzel = self.kuerzel.upper()
        kuerzel = re.sub(r'[^A-Z0-9]', '', kuerzel)
        kuerzel = kuerzel[:6]
        return kuerzel

    def get_safe_semester(self) -> int:
        if str(self.semester).isdigit():
            return int(str(self.semester))
        return 10

    def add_kompetenzniveau(self, val):
        if val not in self.kompetenzniveaus:
            self.kompetenzniveaus.append(val)

    def min_kompetenzniveau(self):
        if len(self.kompetenzniveaus) == 0:
            return 1
        return min(self.kompetenzniveaus)

    def max_kompetenzniveau(self):
        if len(self.kompetenzniveaus) == 0:
            return 4
        return max(self.kompetenzniveaus)

    def kompetenzniveau(self):
        if self.min_kompetenzniveau() != self.max_kompetenzniveau():
            return f'{self.min_kompetenzniveau()} - {self.max_kompetenzniveau()}'
        else:
            return f'{self.min_kompetenzniveau()}'

    def total(self):
        return self.praesenzlektionen_distance_learning + self.praesenzlektionen_vor_ort + self.selbststudium

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return self.__str__()

    def link_handlungskompetenz(self, handlungskompetenz):
        if handlungskompetenz not in self.handlungskompetenzen:
            self.handlungskompetenzen.append(handlungskompetenz)
