import logging
import os
from datetime import datetime
from typing import List
import re

import helpers
import markdown_document_wrapper
from hfmodul import HfModul


def extract_rahmenlehrplan_codes(text):
    # Regular expression to match patterns like B13, B13.1, A1, C22.3, etc.
    pattern = r'[ABC]\d+(?:\.\d+)?'
    # Find all matches in the text
    return re.findall(pattern, text)


def try_find_one_of(patterns: List[str], haystack: str) -> str:
    for pattern in patterns:
        search_result = re.search(pattern, str(haystack))
        if search_result is not None:
            return search_result.group(1)
    return None


def get_hfmoduls(repos) -> List[HfModul]:
    hfmoduls: List[HfModul] = []

    for repo in repos:
        repo_dir = repo["path"]

        mainReadme_success, mainReadme = markdown_document_wrapper.try_load_from_file(
            os.path.join(repo_dir, 'README.md'))
        if not mainReadme_success:
            logging.warning("Missing README.md in root")
            continue

        master_kuerzel_name = str(mainReadme.get_first_header())
        master_kuerzel = master_kuerzel_name.split("-",1 )[0].strip()
        master_name = master_kuerzel_name.split("-", 1)[-1].strip()
        master_beschreibung = mainReadme.get_children_for_header("Kurzbeschreibung des Moduls", nested=True)
        master_transfer = mainReadme.get_children_for_header("Angaben zum Transfer der erworbenen Kompetenzen")

        handlungskomp_doc_success, handlungskomp_doc = markdown_document_wrapper.try_load_from_file(
            os.path.join(repo_dir, '1_Handlungskompetenzen', 'README.md'))
        if not handlungskomp_doc_success:
            logging.warning("Missing README.md in 1_Handlungskompetenzen")
            continue

        berufsspezifische_handlungskomp: markdown_document_wrapper.MistleToeWrapped = handlungskomp_doc.get_children_for_header(
            "Modulspezifische Handlungskompetenzen")
        berufsspezifische_handlungskomp_nested: markdown_document_wrapper.MistleToeWrapped = handlungskomp_doc.get_children_for_header(
            "Modulspezifische Handlungskompetenzen", nested=True)
        kompetenzmatrix: markdown_document_wrapper.MistleToeWrapped = handlungskomp_doc.get_children_for_header(
            "Kompetenzmatrix", nested=False)
        kompetenzmatrix_nested: markdown_document_wrapper.MistleToeWrapped = handlungskomp_doc.get_children_for_header(
            "Kompetenzmatrix", nested=True)
        allgemeine_handlungskomp: markdown_document_wrapper.MistleToeWrapped = handlungskomp_doc.get_children_for_header("Allgemeine Handlungskompetenzen")

        handlungssituationen_doc_success, handlungssituationen_doc = markdown_document_wrapper.try_load_from_file(
            os.path.join(repo_dir, '5_Handlungssituationen', 'README.md'))
        if not handlungskomp_doc_success:
            logging.warning("Missing README.md in 5_Handlungssituationen")
            continue

        handlungssituationen: markdown_document_wrapper.MistleToeWrapped = handlungssituationen_doc.get_children_for_header(
            "Handlungsziele und Handlungssituationen", nested=True)

        organisatorisches_file = os.path.join(repo_dir, "0_Organisatorisches", "README.md")
        organisatorisches_doc_success, organisatorisches_doc = markdown_document_wrapper.try_load_from_file(
            organisatorisches_file)
        if not organisatorisches_doc_success:
            logging.warning("Missing README.md in 0_Organisatorisches")
            continue

        autorenschaft = organisatorisches_doc.get_children_for_header("Autorenschaft")

        repo_umsetzung_path = os.path.join(repo_dir, '3_Umsetzung')

        umsetzung_files = [x for x in os.listdir(repo_umsetzung_path) if x.endswith(".md")]

        for umsetzung_file in umsetzung_files:
            umsetzung_file = os.path.join(repo_umsetzung_path, umsetzung_file)
            umsetzung_doc_success, umsetzung_doc = markdown_document_wrapper.try_load_from_file(umsetzung_file)
            if not umsetzung_doc_success:
                logging.warning(f"Error loading Umsetzung {umsetzung_file}")
                continue

            lektionen = str(umsetzung_doc.get_children_for_header("Lektionen"))
            if lektionen.strip() == "":
                logging.warning(f"missing Lektionen header in {umsetzung_file}. skipping")
                continue

            hfmodul = HfModul(repo["name"], master_kuerzel, master_name)
            main_heading = umsetzung_doc.get_first_header()
            if str(main_heading).lower().strip() != "umsetzung":
                main_heading_str = str(main_heading)
                hfmodul.kuerzel = main_heading_str.split("-", 1)[0].strip()
                hfmodul.name = main_heading_str.split("-", 1)[-1].strip()

            if hfmodul.get_safe_kuerzel() == "NAMEDE":
                hfmodul.kuerzel = hfmodul.safe_code

            main_heading_first_content = umsetzung_doc.get_children_for_header(main_heading.str()).str()

            fahrplan_section = umsetzung_doc.get_children_for_header("Fahrplan")

            praesenzlektionen_preparsed = try_find_one_of([r"Präsenz:\s*(\d+)",
                                                                     r"Präsenzunterricht:\s*(\d+)",
                                                                     r"Präsenzlektionen\ \(Vor Ort\):\s*(\d+)"],
                                                                    str(lektionen))

            if praesenzlektionen_preparsed is not None:
                hfmodul.praesenzlektionen_vor_ort = int(praesenzlektionen_preparsed)
            else:
                logging.warning(f"no valid value for praesenzlektionen_preparsed in {hfmodul.kuerzel}")

            praesenzlektionen_distance_learning_preparsed = try_find_one_of([r"Virtuell:\s*(\d+)",
                                                                     r"Fernunterricht:\s*(\d+)",
                                                                     r"Präsenzlektionen\ \(Distance Learning\):\s*(\d+)"],
                                                                    str(lektionen))

            if praesenzlektionen_distance_learning_preparsed is not None:
                hfmodul.praesenzlektionen_distance_learning = int(praesenzlektionen_distance_learning_preparsed)
            else:
                logging.warning(f"no valid value for praesenzlektionen_distance_learning in {hfmodul.kuerzel}")

            selbststudium_preparsed = re.search(r"Selbststudium:\s*(\d+)", str(lektionen))

            if selbststudium_preparsed is not None:
                hfmodul.selbststudium = int(selbststudium_preparsed.group(1))
            else:
                logging.warning(f"no valid value for selbststudium in {hfmodul.kuerzel}")

            hfmodul.schluesselbegriffe = umsetzung_doc.get_children_for_header("Schlüsselbegriffe")
            hfmodul.beschreibung = master_beschreibung
            hfmodul.fahrplan = fahrplan_section
            hfmodul.handlungssituationen = handlungssituationen
            hfmodul.methoden = umsetzung_doc.get_children_for_header("Methoden")
            hfmodul.lernziele = umsetzung_doc.get_children_for_header("Lernziele")
            if hfmodul.lernziele.empty():
                hfmodul.lernziele = umsetzung_doc.get_children_for_header("Lerninhalte")
                if hfmodul.lernziele.empty():
                    logging.warning(f"Lernziele fehlen in {hfmodul.get_safe_kuerzel()}")
                else:
                    logging.warning(f"{hfmodul.get_safe_kuerzel()}: Falscher Übertitel 'Lerninhalte'. 'Lernziele' ist zu verwenden!")

            hfmodul.semester = markdown_document_wrapper.extract_field(main_heading_first_content, "Semester")
            hfmodul.bereich = markdown_document_wrapper.extract_field(main_heading_first_content, "Bereich")
            hfmodul.dispensation = umsetzung_doc.get_children_for_header("Dispensation")
            hfmodul.lehr_und_lernformen = umsetzung_doc.get_children_for_header("Lehr- und Lernformen")
            hfmodul.repository = repo["url"]
            hfmodul.autor = autorenschaft

            berufsspezifische_handlungskomp_for_module = berufsspezifische_handlungskomp_nested.get_children_for_header(hfmodul.kuerzel)
            if berufsspezifische_handlungskomp_for_module.empty():
                hfmodul.modulspezifische_handlungskompetenzen = berufsspezifische_handlungskomp
            else:
                hfmodul.modulspezifische_handlungskompetenzen = berufsspezifische_handlungskomp_for_module
            hfmodul.allgemeine_handlungskompeten = allgemeine_handlungskomp
            hfmodul.kompetenzmatrix = kompetenzmatrix
            hfmodul.kompetenzmatrix_nested = kompetenzmatrix_nested

            hfmodul.modulspezifische_handlungskompetenzen_nums = extract_rahmenlehrplan_codes(str(hfmodul.modulspezifische_handlungskompetenzen))
            hfmodul.allgemeine_handlungskompeten_nums = extract_rahmenlehrplan_codes(str(hfmodul.allgemeine_handlungskompeten))
            hfmodul.voraussetzungen = umsetzung_doc.get_children_for_header("Voraussetzungen")
            hfmodul.lehrmittel = umsetzung_doc.get_children_for_header("Lehrmittel")

            hfmodul.transfer = master_transfer
            spezifischer_transfer = umsetzung_doc.get_children_for_header("Angaben zum Transfer der erworbenen Kompetenzen")
            if not spezifischer_transfer.empty():
                hfmodul.transfer = spezifischer_transfer

            spezifischer_transfer = umsetzung_doc.get_children_for_header("Transfer")
            if not spezifischer_transfer.empty():
                hfmodul.transfer = spezifischer_transfer

            hfmodul.last_change = helpers.today_str()
            hfmoduls.append(hfmodul)

    return hfmoduls