import json
import logging
from pprint import pprint
from typing import List

import app_data
import app_hfmodules
import app_rahmenlehrplan
import app_render
import config
import repo_helpers
from hfmodul import HfModul
from lernstunden import Lernstunden
import semester

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT, force=True)

# Clones every repository configured in the config and places them in the work directory. Fetching changes is only required for debugging purposes.
repos = repo_helpers.create_or_update_repos(no_update=True)

# Parses the content from specified markdown files in each repository, prepares the content, and maps the information into the hfmodul class.
hfmoduls: List[HfModul] = app_hfmodules.get_hfmoduls(repos)
# Mapping the modules of the "Handlungskompetenzen" for each module to the "Rahmenlehrplan."
rahmenlehrplan = app_rahmenlehrplan.map_modules_with_rahmenlehrplan(hfmoduls)
app_rahmenlehrplan.calculate_total_reinject_into_handlungskompetenzen(rahmenlehrplan)
lernstunden_per_kompetenzbereich = app_rahmenlehrplan.calculate_anteil_lernstunden_per_kompetenzbereich(rahmenlehrplan)
total_lernstunden_checksum = sum([lc["anteil_lernstunden"] for lc in lernstunden_per_kompetenzbereich])

if 1 - total_lernstunden_checksum > 0.00001:
    logging.error("Lernstunden do not add up to 100%")

# The modules are displayed in sections, with each section representing a semester.
semesters = semester.split_hfmoduls_into_semesters(hfmoduls, 1, 5)
# semesters.append(semester.Semester(hfmoduls, None))
# semester.inject_spezmoduls_into_semesters(semesters, config.spezmoduls)

# Happy rendering

app_render.render_htmls(hfmoduls)
app_render.render_pdfs(hfmoduls)
app_render.render_overview(semesters)
app_render.render_overview_csv(hfmoduls)

ist_lernstunden = Lernstunden()
ist_lernstunden.calculate_and_update(hfmoduls)
diff_lernstunden = ist_lernstunden - config.soll_lernstunden

app_render.render_lessons(ist_lernstunden)
app_render.render_lessons_with_diff(ist_lernstunden, config.soll_lernstunden, diff_lernstunden, lernstunden_per_kompetenzbereich)

app_render.render_rahmenlehrplan(rahmenlehrplan)

app_render.copy_assets()

app_data.create_json(hfmoduls)

logging.info("Done")



