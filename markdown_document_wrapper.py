import json
import os
import re
from typing import List, Self

import mistletoe.block_token
from mistletoe import HTMLRenderer
from mistletoe.block_token import Document
from mistletoe.token import Token

from handlungskompetenz import Handlungskompetenz


def get_text_from_header(heading_token: mistletoe.block_token.Heading) -> str:
    raw_texts = [x.content for x in heading_token.children if isinstance(x, mistletoe.span_token.RawText)]
    return "\n".join(raw_texts)


def get_text_from_tokens(tokens: List[mistletoe.token], token: mistletoe.token = None) -> str:
    if token is not None:
        if isinstance(token, mistletoe.span_token.RawText):
            return token.content
        if hasattr(token, "children") and token.children is not None and len(token.children) == 0:
            return ""
        if hasattr(token, "children") and token.children is not None:
            sub_strs = []
            for child in token.children:
                inner_text = get_text_from_tokens(tokens, child)
                if isinstance(child, mistletoe.block_token.ListItem):
                    inner_text = " - " + inner_text
                sub_strs.append(inner_text)
            return "\n".join(sub_strs)
        return ""
    else:
        sub_strs = []
        for child in tokens:
            sub_strs.append(get_text_from_tokens(tokens, child))
        return "\n".join(sub_strs)


def clean_header_name(header_name: str) -> str:
    return header_name.strip().rstrip(":").lower()


def extract_field(text, field_name):
    # Regular expression to find the field and capture its value
    match = re.search(fr"{field_name}:\s*(.+)", text)
    if match:
        return match.group(1).strip()
    else:
        return None


class MistleToeWrapped:

    def __init__(self, children: List[Token]):
        self.children = children

    def str(self):
        return self.__str__()

    def __str__(self):
        return get_text_from_tokens(self.children)

    def __repr__(self):
        return self.__str__()

    def empty(self):
        return len(self.children) == 0

    def get_children_for_header(self, header: str, nested: bool = False) -> Self:
        children: List[Token] = []
        found = False
        level = 0
        for c in self.children:
            if (not found and isinstance(c, mistletoe.block_token.Heading)
                    and clean_header_name(header) == clean_header_name(get_text_from_header(c))):
                found = True
                level = c.level
            elif found:
                if not isinstance(c, mistletoe.block_token.Heading):
                    children.append(c)
                elif isinstance(c, mistletoe.block_token.Heading) and c.level > level and nested:
                    children.append(c)
                else:
                    break
        return MistleToeWrapped(children)

    def extract_content(self, token):
        """
        Walks through the children of a token recursively and extracts the content by always
        taking the first child until we reach a token with a 'content' field.
        """
        while hasattr(token, 'children') and not hasattr(token, 'content'):
            if len(token.children) > 0:
                token = token.children[0]
            else:
                return ""
        return getattr(token, 'content', '')


    def find_first_of_type(self, token, target_type):
        """
        Walks through the children of a token recursively and finds the first instance of a specified type.
        Iterates through all children, not just the first one.

        Args:
            token: The root token to start the search from.
            target_type: The type to look for (e.g., mistletoe.block_token.Table).

        Returns:
            The first instance of the target_type found, or None if no such instance is found.
        """
        if isinstance(token, target_type):
            return token

        if isinstance(token, list):
            for t in token:
                result = self.find_first_of_type(t, target_type)
                if result:
                    return result  # Return the first found instance

        if hasattr(token, 'children') and token.children is not None:
            for child in token.children:
                result = self.find_first_of_type(child, target_type)
                if result:
                    return result  # Return the first found instance

        return None

    def extract_table(self):
        table_data = {"header":  [], "body": []}

        table = self.find_first_of_type(self.children, mistletoe.block_token.Table)

        table = [table, ]

        for token in table:
            if isinstance(token, mistletoe.block_token.Table):
                # Extract header

                table_data["header"] = [self.extract_content(cell) for cell in token.header.children]

                # Extract rows
                for row in token.children:
                    row_data = [self.extract_content(cell) for cell in row.children]
                    table_data["body"].append(row_data)

        return table_data

    def html(self):
        with HTMLRenderer(max_line_length=20) as renderer:
            document = mistletoe.Document("")
            document.children = self.children
            return renderer.render(document)


def serialize(obj):

    if isinstance(obj, MistleToeWrapped):
        return str(obj)

    if isinstance(obj, Handlungskompetenz):
        return str(obj)

    return obj.__dict__


class MarkdownDocumentWrapper:

    def __init__(self, document: Document):
        self.document = document

    def get_first_header(self) -> MistleToeWrapped:
        headings = [x for x in self.document.children if isinstance(x, mistletoe.block_token.Heading)]
        if len(headings) == 0:
            return None
        return MistleToeWrapped([headings[0]])

    def get_children_for_header(self, header: str, nested: bool = False) -> MistleToeWrapped:
        children: List[Token] = []
        found = False
        level = 0
        for c in self.document.children:
            if (not found and isinstance(c, mistletoe.block_token.Heading)
                    and clean_header_name(header) == clean_header_name(get_text_from_header(c))):
                found = True
                level = c.level
            elif found:
                if not isinstance(c, mistletoe.block_token.Heading):
                    children.append(c)
                elif isinstance(c, mistletoe.block_token.Heading) and c.level > level and nested:
                    children.append(c)
                else:
                    break
        return MistleToeWrapped(children)


def try_load_from_file(file_path: str) -> (bool, MarkdownDocumentWrapper):
    if not os.path.exists(file_path) or not os.path.isfile(file_path):
        return False, None
    with open(os.path.join(file_path), 'r', encoding="utf-8") as fin:
        rendered = MarkdownDocumentWrapper(mistletoe.Document(fin))
        return True, rendered
    return False, None
