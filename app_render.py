import logging
import os
import shutil
from typing import List

import jinja2
from jinja2 import FileSystemLoader

import config
import helpers
from hfmodul import HfModul
from lernstunden import Lernstunden
from semester import Semester

jenv = jinja2.Environment(loader=FileSystemLoader("templates/"))


def check_output_folder():
    if not os.path.exists(config.HTML_OUT_DIR):
        os.mkdir(config.HTML_OUT_DIR)


def render_htmls(hfmoduls: List[HfModul]):
    global jenv
    check_output_folder()
    for hfmodul in hfmoduls:
        # template = jenv.get_template("modul.jinja2")
        template = jenv.get_template("modul.jinja2")
        content = template.render(hfm=hfmodul, helpers=helpers)
        hfmodul_filename = os.path.join(config.HTML_OUT_DIR, hfmodul.get_safe_kuerzel() + ".html")
        with open(hfmodul_filename, "w", encoding="utf-8") as fp:
            fp.write(content)
        logging.info(f"Created {hfmodul_filename}")


def render_pdfs(hfmoduls: List[HfModul]):
    global jenv
    check_output_folder()
    download_script_lines = []
    for hfmodul in hfmoduls:
        hfmodul_filename_pdf = hfmodul.get_safe_kuerzel() + ".pdf"
        hfmodul_path_pdf = os.path.join(config.HTML_OUT_DIR, hfmodul_filename_pdf)
        template_pdf = jenv.get_template("modul.pdf.jinja2")
        content_pdf = template_pdf.render(hfm=hfmodul, helpers=helpers)
        helpers.safe_to_pdf(content_pdf, hfmodul_path_pdf)
        logging.info(f"Created {hfmodul_path_pdf}")
        download_script_lines.append(f"curl -L {config.base_url}{hfmodul_filename_pdf} -o {hfmodul_filename_pdf}\n")
    with open(os.path.join(config.HTML_OUT_DIR, "download_pdfs.sh"), "w", encoding="utf-8") as fp:
        fp.writelines(download_script_lines)



def copy_assets():
    shutil.copy("title.png", "public")
    logging.info(f"Copied assets")

def render_overview(semesters: List[Semester]):
    check_output_folder()
    sorted_semesters = sorted(semesters, key=lambda module: module.semester())
    overview_filename = os.path.join(config.HTML_OUT_DIR, "index.html")
    template = jenv.get_template("overview.jinja2")
    content = template.render(sems=sorted_semesters, now=helpers.now_str())
    with open(overview_filename, "w", encoding="utf-8") as fp:
        fp.write(content)
    logging.info(f"Created {overview_filename}")


def render_lessons(ist: Lernstunden):
    overview_filename = os.path.join(config.HTML_OUT_DIR, "lessons.html")
    template = jenv.get_template("lessonscalculation.jinja2")
    content = template.render(ls=ist,
                              now=helpers.now_str())
    with open(overview_filename, "w", encoding="utf-8") as fp:
        fp.write(content)
    logging.info(f"Created {overview_filename}")


def render_lessons_with_diff(ist: Lernstunden, soll: Lernstunden, diff: Lernstunden, lernstunden_per_kompetenzbereich):
    overview_filename = os.path.join(config.HTML_OUT_DIR, "lessonsdiff.html")
    template = jenv.get_template("lessonscalculationdiff.jinja2")
    content = template.render(ls=ist,
                              soll=soll,
                              diff=diff,
                              kompetenzbereiche_names=config.kompetenzbereiche_names,
                              lernstunden_per_kompetenzbereich=lernstunden_per_kompetenzbereich,
                              now=helpers.now_str())
    with open(overview_filename, "w", encoding="utf-8") as fp:
        fp.write(content)
    logging.info(f"Created {overview_filename}")


def render_rahmenlehrplan(rahmenlehrplan):
    overview_filename = os.path.join(config.HTML_OUT_DIR, "rahmenlehrplan.html")
    template = jenv.get_template("rahmenlehrplan.jinja2")
    content = template.render(rlcs=rahmenlehrplan, now=helpers.now_str())
    with open(overview_filename, "w", encoding="utf-8") as fp:
        fp.write(content)
    logging.info(f"Created {overview_filename}")


def render_overview_csv(hfmoduls: List[HfModul]):
    overview_filename = os.path.join(config.HTML_OUT_DIR, "overview.csv")
    with open(overview_filename, "w", encoding="utf-8") as fp:
        fp.write("Kuerzel|Name|Semester|Bereich|Lektionen vor Ort|Lektionen Distance|Lektionen Präsenz|Lektionen "
                 "Selbststudium|\n")
        for hfmodul in hfmoduls:
            fp.write(f"{hfmodul.get_safe_kuerzel()}|{hfmodul.name}|{hfmodul.semester}|")
            fp.write(f"{hfmodul.bereich}|")
            fp.write(f"{hfmodul.praesenzlektionen_vor_ort}|")
            fp.write(f"{hfmodul.praesenzlektionen_distance_learning}|")
            fp.write(f"{hfmodul.praesenzlektionen_distance_learning+hfmodul.praesenzlektionen_vor_ort}|")
            fp.write(f"{hfmodul.selbststudium}|")
            fp.write("\n")