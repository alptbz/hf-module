import logging
import re
from pprint import pprint
from typing import List

from handlungskompetenz import Handlungskompetenz
from hfmodul import HfModul

PARENT_MARKER = " *"
CHILDREN_MARKER = "   *"


def parse_handlungskompetenz(in_str: str) -> (str, str):
    number_extract_pattern = r"[A-Z][0-9]+(\.[0-9]+)?"
    h_num_res = re.search(number_extract_pattern, in_str)
    if h_num_res is None:
        return None, None
    h_num = h_num_res.group()
    h_text = in_str.replace(f"{PARENT_MARKER} {h_num}", "", 1).strip()
    return h_num, h_text


def extract_category(in_str: str) -> str:
    number_extract_pattern = r"[A-Z][0-9]+\."
    h_num_res = re.search(number_extract_pattern, in_str)
    if h_num_res is None:
        return None
    h_num = h_num_res.group().rstrip(".")
    return h_num


def extract_niveau(in_str: str) -> str:
    pattern = r"Niveau: (\d)"
    return int(re.findall(pattern, in_str)[0])


def get_rahmenlehrplan() -> dict[str, Handlungskompetenz]:
    with open("rahmenlehrplan.md", "r", encoding="utf-8") as file:
        rahmenlehrplan_lines = file.readlines()
    rahmenlehrplan_dict = {}
    current_block = None
    for line in rahmenlehrplan_lines:
        parsed_line_num, parsed_line_text = parse_handlungskompetenz(line)
        if parsed_line_num is None:
            continue
        if line.startswith(PARENT_MARKER):
            current_block = parsed_line_num
            if current_block not in rahmenlehrplan_dict:
                rahmenlehrplan_dict[current_block] = Handlungskompetenz(id=current_block, description=parsed_line_text, niveau=0, is_main=True)
        elif line.startswith(CHILDREN_MARKER) and current_block is not None:
            extracted_niveau = extract_niveau(line)
            rahmenlehrplan_dict[current_block].add_child(Handlungskompetenz(id=parsed_line_num,
                                                                            description=parsed_line_text,
                                                                            niveau=int(extracted_niveau)))

    return rahmenlehrplan_dict


def map_modules_with_rahmenlehrplan(hfmoduls: List[HfModul]) -> dict[str, Handlungskompetenz]:
    rahmenlehrplan = get_rahmenlehrplan()
    for hfmodul in hfmoduls:
        handlungskompetenzen = hfmodul.modulspezifische_handlungskompetenzen_nums
        for handlungskomp in handlungskompetenzen:
            category = handlungskomp
            if category not in rahmenlehrplan:
                category = extract_category(handlungskomp)
            if category not in rahmenlehrplan:
                logging.warning(f"Unable to parse invalid Handlungskompetenz in {hfmodul.safe_code} {handlungskomp}")
                continue
            if category == handlungskomp:
                if hfmodul not in rahmenlehrplan[category].moduls:
                    rahmenlehrplan[category].link_module(hfmodul)
            else:
                child = [c for c in rahmenlehrplan[category].children if c.id == handlungskomp][0]
                if hfmodul not in child.moduls:
                    child.link_module(hfmodul)
                hfmodul.kompetenzniveaus.append(rahmenlehrplan[category].get_child(handlungskomp).niveau)
    distribute_lernstunden_to_rahmenlehrplan(hfmoduls)
    return rahmenlehrplan


def distribute_lernstunden_to_rahmenlehrplan(hfmoduls: List[HfModul]):
    for hfmodul in hfmoduls:
        main_handlungskompetenzen: List[Handlungskompetenz] = [h for h in hfmodul.handlungskompetenzen if h.is_main]
        num_of_handlungskompetenzen = len(main_handlungskompetenzen)
        logging.debug(f'{hfmodul.get_safe_kuerzel()} has {num_of_handlungskompetenzen} Handlungskompetenzen')
        for hk in main_handlungskompetenzen:
            hk.add_lernstunden(
                praesenzlektionen_vor_ort= hfmodul.praesenzlektionen_vor_ort / num_of_handlungskompetenzen,
                selbststudium= hfmodul.selbststudium / num_of_handlungskompetenzen,
                praesenzlektionen_distance_learning= hfmodul.praesenzlektionen_distance_learning / num_of_handlungskompetenzen
            )


def calculate_total_reinject_into_handlungskompetenzen(handlungskompetenzen: dict[str, Handlungskompetenz]) -> None:
    total_praesenzlektionen_vor_ort: float = 0
    total_selbststudium: float = 0
    total_praesenzlektionen_distance_learning: float = 0
    for handlungskompetenz_id in handlungskompetenzen:
        hk: Handlungskompetenz = handlungskompetenzen[handlungskompetenz_id]
        total_praesenzlektionen_vor_ort += hk.total_praesenzlektionen_vor_ort
        total_selbststudium += hk.total_selbststudium
        total_praesenzlektionen_distance_learning += hk.total_praesenzlektionen_distance_learning
    total_lernstunden = total_selbststudium + total_praesenzlektionen_vor_ort + total_praesenzlektionen_distance_learning
    for handlungskompetenz_id in handlungskompetenzen:
        hk: Handlungskompetenz = handlungskompetenzen[handlungskompetenz_id]
        hk.anteil_total_lernstunden = hk.total_lernstunden / total_lernstunden


def calculate_anteil_lernstunden_per_kompetenzbereich(handlungskompetenzen: dict[str, Handlungskompetenz]):
    hks: List[Handlungskompetenz] = handlungskompetenzen.values()
    categories_ids = list(set([h.category for h in hks]))
    categories_ids.sort()
    categories = []
    for c_id in categories_ids:
        categories.append({"id": c_id,
                           "anteil_lernstunden": sum([h.anteil_total_lernstunden for h in hks if h.category == c_id])})
    return categories


if __name__ == "__main__":
    pprint(get_rahmenlehrplan())