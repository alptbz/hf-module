from typing import List
from hfmodul import HfModul
from spezmodul import SpezModul


class Semester:

    def __init__(self, hfmoduls: List[HfModul], semester_num: int):
        self.semester_num = semester_num
        self.hfmoduls: List[HfModul] = []
        for hfmodul in hfmoduls:
            if str(hfmodul.semester) == str(semester_num):
                self.hfmoduls.append(hfmodul)
        self.spezmoduls: List[SpezModul] = []

    def semester(self):
        if self.semester_num is None:
            return -1
        return self.semester_num

    def semester_str(self):
        if self.semester_num is None:
            return "N/A"
        return self.semester_num

    def total_vorort(self) -> int:
        return sum([h.praesenzlektionen_vor_ort for h in self.hfmoduls])

    def total_distance(self) -> int:
        return sum([h.praesenzlektionen_distance_learning for h in self.hfmoduls])

    def total_selbststudium(self) -> int:
        return sum([h.selbststudium for h in self.hfmoduls])

    def total_total_spez(self):
        return sum([h.total_lektionen for h in self.spezmoduls])

    def total(self) -> int:
        return self.total_vorort() + self.total_distance() + self.total_selbststudium() + self.total_total_spez()


def split_hfmoduls_into_semesters(hfmoduls: List[HfModul], from_semester: int, to_semester: int) -> List[Semester]:
    semesters = []
    for i in range(from_semester, to_semester+1):
        semesters.append(Semester(hfmoduls, i))
    return semesters


def inject_spezmoduls_into_semesters(semesters: List[Semester], spezmoduls: List[SpezModul]):
    for spez in spezmoduls:
        semester = next((n for n in semesters if str(n.semester_num) == str(spez.semester)), None)
        if semester is None:
            continue
        semester.spezmoduls.append(spez)
