from typing import List, Self

from hfmodul import HfModul


class Lernstunden:

    def __init__(self):
        self.vorort: int = 0
        self.distance: int = 0
        self.selbststudium: int = 0
        self.present: int = 0
        self.total: int = 0
        self.ind_selbststudium: int = 0
        self.qualification: int = 0
        self.total_incl_praxis: int = 0
        self.praxis: int = 0


    def reset(self):
        self.vorort: int = 0
        self.distance: int = 0
        self.present: int = 0
        self.selbststudium: int = 0
        self.total: int = 0
        self.ind_selbststudium: int = 225
        self.qualification = 5 * 50 + 100
        self.total_incl_praxis = 0
        self.praxis = 720

    def calculate_and_update(self, hfmoduls: List[HfModul]):
        self.reset()
        for hfmodul in hfmoduls:
            self.vorort += hfmodul.praesenzlektionen_vor_ort
            self.distance += hfmodul.praesenzlektionen_distance_learning
            self.selbststudium += hfmodul.selbststudium
        self.present = self.vorort + self.distance
        self.total = self.present + self.selbststudium + self.ind_selbststudium + self.qualification
        self.total_incl_praxis = self.total + self.praxis

    def __add__(self, other):
        n = Lernstunden()
        n.vorort = self.vorort + other.vorort
        n.distance = self.distance + other.distance
        n.present = self.present + other.present
        n.selbststudium = self.selbststudium + other.selbststudium
        n.total = self.total + other.total
        n.ind_selbststudium = self.ind_selbststudium + other.ind_selbststudium
        n.qualification = self.qualification + other.qualification
        n.total_incl_praxis = self.total_incl_praxis + other.total_incl_prais
        n.praxis = self.praxis + other.praxis
        return n

    def __sub__(self, other):
        n = Lernstunden()
        n.vorort = self.vorort - other.vorort
        n.distance = self.distance - other.distance
        n.present = self.present - other.present
        n.selbststudium = self.selbststudium - other.selbststudium
        n.total = self.total - other.total
        n.ind_selbststudium = self.ind_selbststudium - other.ind_selbststudium
        n.qualification = self.qualification - other.qualification
        n.total_incl_praxis = self.total_incl_praxis - other.total_incl_praxis
        n.praxis = self.praxis - other.praxis
        return n