# Instruktionen - Anforderungen an Modulrepository

## /README.md
 - Titelzeile (erstes `#` Heading muss Titel beinhalten) in Format: `[Kürzel] - [Modulname]`<br/> Beispiel: `FAAS - Function as a Service`
 - Heading mit `Kurzbeschreibung des Moduls` Anschliessende Paragraph wird als Inhalt verwendet
 - `Angaben zum Transfer der erworbenen Kompetenzen` muss vorhanden sein (Wie werden die Handlungsziele / Kompetenzen dem/der Studierendem vermittelt)

## Umsetzung/*
 - Im Ordner `3_Umsetzung` werden alle `.md` Dateien eingelesen, welche das Stichwort `Lektionen` im einem Heading enthalten.
 - Pro gültige Datei wird ein Modul angelegt
 - Wenn ein Dokument eine gültige Modulbezeichnung beinhaltet (siehe `/README.md`) wird dieser Titel erstellt
 - Direkt nach dem ersten Titel folgt ein Paragraf mit einer Liste. Diese beinhaltet `Bereich` und `Semester`. Beispiel:
```markdown
 - Bereich: System- und Netzwerkarchitektur bestimmen
 - Semester: 1 
```
 - Auf Heading `Lektionen` folgt ein Paragraf mit einer Liste, Beispiel:
```markdown
    * Präsenzlektionen (Vor Ort): 20
    * Präsenzlektionen (Distance Learning): 15
    * Selbststudium: 15
```
 - Auf Heading `Voraussetzungen` folgt ein Paragraf mit einer Zeile mit den Voraussetzungen für das Modul
 - Auf Heading `Dispensation` folgt ein Paragraf mit einer Zeile mit den Dispensationsgründen für das Modul
 - Auf Heading `Methoden` folgt ein Paragraf mit einer Zeile mit den verwendenten Methoden im Modul
 - Auf Heading `Lernziele` folgt ein Paragraf mit einer Liste von Lerninhalten
 - Auf Heading `Übungen und Praxis` folgt ein Paragraf mit einer Liste von Übungen und Praxisaufgaben
 - Auf Heading `Lehr- und Lernformen` folgt ein Paragraf mit einer Zeile von Lehr- und Lernformen
 - Auf Heading `Lehrmittel` folgt eine Liste von Lehrmittel. Diese wird jedoch nicht beachtet, sondern es wird ein automatischer Text eingefügt, welcher auf das Repository verweist. 

## 1_Handlungskompetenzen/README.md
 - Auf Heading `Modulspezifische Handlungskompetenzen` folgt eine Markdown Liste im einem spezifischen Format. Beispiel:
```markdown
* B12: System- und Netzwerkarchitektur bestimmen
  * B12.1: Die bestehende Systemarchitektur beurteilen und weiterentwickeln 
  * B12.2: Die bestehende Netzwerk-Architektur analysieren, Umsetzungsvarianten definieren und eine Soll-Architektur entwickeln
  * B12.3: Bestehende ICT Konfigurationen analysieren, Umsetzungsvarianten für die Erweiterung definieren und Soll-Konfigurationen entwickeln
  * B12.4: Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen
```
 - Vorlagen zum Copy Pasten: [rahmenlehrplan](rahmenlehrplan.md)

**Hinweis: Eventuell sind die Instruktionen nicht vollständig. Immer überprüfen, ob der Output (Webseite / PDFs) stimmt.**