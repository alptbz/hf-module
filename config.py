from lernstunden import Lernstunden
from spezmodul import SpezModul

WORKDIR_PATH = "./workdir"

REPOS = [
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/prj"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/IaC"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/maas"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/aws-base"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/azure"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/PE"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/NWA"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/BPM"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/Eco"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/FAAS"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/ITSM"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/MSVC"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/NoSQL"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/CSec"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/SQL"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/DSG"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/DSEC"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/devops"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/IaCA"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/CnC"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/CnA"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/CnE"},
    {"url": "https://gitlab.com/ch-tbz-wb/Stud/Inno"}
]

HTML_OUT_DIR = "public"

soll_lernstunden = Lernstunden()
soll_lernstunden.vorort = 500
soll_lernstunden.present = 1500
soll_lernstunden.selbststudium = 400
soll_lernstunden.ind_selbststudium = 200
soll_lernstunden.total = 2880
soll_lernstunden.praxis = 720
soll_lernstunden.total_incl_praxis = 3600

spezmoduls = [
    SpezModul("SEM1", "Semesterarbeit 1", 1, 50 + 35),
    SpezModul("SEM2", "Semesterarbeit 2", 2, 50 + 35),
    SpezModul("SEM3", "Semesterarbeit 3", 3, 50 + 35),
    SpezModul("SEM4", "Semesterarbeit 4", 4, 50 + 35),
    SpezModul("SEM5", "Semesterarbeit 5", 5, 50 + 35),
    SpezModul("DIPL", "Diplomarbeit", 6, 100 + 35),
]

kompetenzbereiche_names = {
    "A": "Allgemeine Handlungskompetenzen",
    "B": "Berufsspezifische Handlungskompetenzen",
    "C": "Schwerpunkt des Bildungsanbieters"
}

base_url = "https://ch-tbz-wb.gitlab.io/general/hf-moduls/"