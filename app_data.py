import json
import logging
import os
from typing import List

import config
import markdown_document_wrapper
from hfmodul import HfModul


def create_json(hfmoduls: List[HfModul]):
    json_api_path = os.path.join(config.HTML_OUT_DIR, "hfmodules.json")
    with open(json_api_path, "w", encoding="utf-8") as fp:
        fp.write(json.dumps([hfm.__dict__ for hfm in hfmoduls], default=markdown_document_wrapper.serialize))
    logging.info(f"Created {json_api_path}")