 * A1 Unternehmens- und Führungsprozesse gestalten und verantworten
   * A1.1 Geschäftsprozesse des Unternehmens verantwortungsvoll ausführen (Niveau: 2)
   * A1.2 Unternehmensprozesse überprüfen und zu Handen der Entscheidungsträger überzeugende Vorschläge zur Optimierung unterbreiten (Niveau: 3)
   * A1.3 Fachliche Kenntnisse kombiniert mit betriebswirtschaftlichem Wissen für einen ökonomisch, ökologisch und sozial erfolgreichen Geschäftsgang einsetzen (Niveau: 2)
   * A1.4 Transformationsprozesse im Bereich neuer Technologien, neuer Geschäftsmodelle, Reorganisationen oder Geschäftsprozessinnovationen mitgestalten, mittragen und umsetzen (Niveau: 3) 
   * A1.5 Rechtliche Grundlagen, Regelungen und Normen, die für die Arbeitsumgebung und Produkte relevant sind, beachten und umsetzen (Niveau: 3)
   * A1.6 Arbeitspsychologische Grundsätze im Umgang mit Mitarbeiterinnen und Mitarbeitern berücksichtigen sowie sozial und verantwortungsvoll handeln (Niveau: 3)
   * A1.7 Zusammenarbeit im Team gestalten, reflektieren und Regeln vereinbaren (Niveau: 3)
   * A1.8 Die Führungsrolle in der Organisation wahrnehmen und ausgestalten (Niveau: 3)
   * A1.9 Interpersonelle Konflikte und schwierige individuelle Situationen erkennen, ansprechen und konstruktiv an Lösungen mitarbeiten (Niveau: 3)
   * A1.10 Die Kommunikation und Zusammenarbeit unter Berücksichtigung relevanter Genderfragen, der Diversität und interkulturellen Gegebenheiten gestalten (Niveau: 3) 
   * A1.11 Die Motivation im Team fördern und dieses zu Höchstleistungen befähigen (Niveau: 3)
   * A1.12 Kundenbeziehungen gestalten (Niveau: 3)

 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten (Niveau: 3)
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren (Niveau: 3)
   * A2.2 Das Interesse von Adressaten gewinnen und glaubwürdig sowie überzeugend kommunizieren (Niveau: 3)
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen (Niveau: 3)
   * A2.4 Arbeitsergebnisse mit geeigneten medialen und rhetorischen Elementen zielgruppenadäquat präsentieren (Niveau: 3) 
   * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren (Niveau: 3)
   * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren (Niveau: 3)
   * A2.7 Berichte professionell und in einer für die Adressaten verständlichen Weise verfassen (Niveau: 3)
   * A2.8 Im Arbeitsumfeld mündlich wie schriftlich in Englisch auf Niveau B2 kommunizieren (Niveau: 1)

 * A3 Die persönliche Entwicklung reflektieren und aktiv gestalten (Niveau: 3)
   * A3.1 Die eigenen Kompetenzen bezüglich der beruflichen Anforderungen regelmässig reflektieren, bewerten und daraus den Lernbedarf ermitteln (Niveau: 3)
   * A3.2 Neues Wissen mit geeigneten Methoden erschliessen und arbeitsplatznahe Weiterbildung realisieren (Niveau: 3)
   * A3.3 Neue Technologien kritisch reflexiv beurteilen, adaptieren und integrieren (Niveau: 3)
   * A3.4 Die eigenen digitalen Kompetenzen kontinuierlich weiterentwickeln (Niveau: 3)
   * A3.5 Das eigene Denken, Fühlen und Handeln reflektieren und geeignete persönliche Entwicklungsmassnahmen definieren und umsetzen (Niveau: 3)

 * B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
   * B4.1 ICT-Innovation methodisch mitgestalten (Niveau: 3)
   * B4.2 ICT-Problemstellungen unter Berücksichtigung vernetzten Denkens, Entwicklung neuer ICT-Lösungen und Anwendung aktueller Technologien identifizieren, analysieren und lösen (Niveau: 3)
   * B4.3 Kreative situativ passende ICT-Lösungen für komplexe Probleme mit ineinandergreifenden Einflussgrössen entwickeln (Niveau: 4)
   * B4.4 Geeignete Methoden der Entscheidungsfindung aufgrund der Kriterien- und Argumentationsanalyse anwenden (Niveau: 3)
   * B4.5 Ganzheitliche ICT-Lösungsansätze unter Berücksichtigung von technischen, sozialen, gesellschaftlichen, ethischen, ökologischen und ökonomischen Aspekten entwickeln (Niveau: 3)
   * B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (Niveau: 3)
   * B4.7 Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen (Niveau: 3)

 * B5 ICT-Projekte und Vorhaben planen, leiten, umsetzen und evaluieren 
   * B5.1 ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern (Niveau: 3)
   * B5.2 Sich gegenseitig beeinflussende Faktoren berücksichtigen und Veränderungen antizipieren (Niveau: 3)
   * B5.3 Die Erfolgsfaktoren, die Zusammenarbeit im Team, die Planung der Ressourcen, die Umweltbelastung und die Kostenkontrolle berücksichtigen (Niveau: 2)
   * B5.4 Eine technische Risikoanalyse durchführen und die Ergebnisse in der Planung berücksichtigen (Niveau: 3)
   * B5.5 Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei der Durchführung zeigen (Niveau: 3)
   * B5.6 In interdisziplinären Projekten teamorientiert handeln (Niveau: 3)
   * B5.7 Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren, entwickeln (Niveau: 3)
   * B5.8 Verhältnis zwischen Kosten und Terminen nach Vorgaben optimieren (Niveau: 2)
   * B5.9 Dokumente, die die Überwachung des Projektfortschritts erleichtern, erstellen und pflegen (Niveau: 3)
   * B5.10 ICT-Projekte termin- und budgetgerecht und in Übereinstimmung mit den ursprünglichen Anforderungen abschliessen (Niveau: 3)

 * B6 Eine ICT-Organisationseinheit leiten
   * B6.1 Aus dem Unternehmensleitbild und der ICT-Strategie die Anforderungen und Rahmenbedingungen ableiten und diese in der technischen ICT-Organisationseinheit konkret umsetzen (Niveau: 3)
   * B6.2 Ressourcen für eine ICT-Organisationseinheit planen und budgetieren, den Mitarbeitereinsatz organisieren und die Kommunikation zu den beteiligten Stakeholdern sicherstellen (Niveau: 3)
   * B6.3 Den Informationsbedarf für Entscheidungssituationen aufgrund von Anspruchsgruppen bestimmen (Niveau: 2)
   * B6.4 Informationstechnologien und -methoden, Marktinformationen und Umfeld (Konkurrenz, Forschung etc.) in der ICT beobachten und bewerten (Niveau: 3)
   * B6.5 Risiken einer ICT-Abteilung analysieren und geeignete Massnahmen ableiten (Niveau: 3)

 * B7 Technische Anforderungen analysieren und bestimmen
   * B7.1 Die ICT-Architektur zielorientiert (ICT Strategie) analysieren, beurteilen und bestimmen (Niveau: 4)
   * B7.2 Geschäftsprozesse aus ICT-Sicht priorisieren, analysieren, spezifizieren und optimieren (Niveau: 3)
   * B7.3 Technische Anforderungen aufnehmen und spezifizieren (Niveau: 3)
   * B7.4 Den Einsatz von geschäftsrelevanten Systemen konzipieren (Niveau: 3)
   * B7.5 Anforderungen für den Einsatz von Informatikmitteln spezifizieren (Niveau: 3)

 * B8 ICT-Qualität sicherstellen
   * B8.1 Das ICT-Qualitätsmanagement-System definieren, dokumentieren, umsetzen und überwachen (Niveau: 3)
   * B8.2 Optimierungen im Qualitätsmanagement planen und umsetzen (Niveau: 3)
   * B8.3 Qualitäts-Messsysteme mit Indikatoren festlegen (Niveau: 3)

 * B9 Datenschutz und Datensicherheit gewährleisten
   * B9.1 ICT-Sicherheitskonzepte zur Gewährleistung der ICT-Sicherheit wie Datenschutz, Datensicherheit und Verfügbarkeit einhalten, umsetzen und unterhalten (Niveau: 3)
   * B9.2 Datensammlungen von Unternehmen erfassen, zu schützende Daten identifizieren und den Schutzbedarf ermitteln (Niveau: 3)
   * B9.3 Sicherheitsrelevante Bausteine vernetzter ICTInfrastrukturen identifizieren, die Gefährdungslage beurteilen und geeignete organisatorische, personelle, infrastrukturelle und technische Schutzmassnahmen ableiten (Niveau: 3)

 * B10 Softwarearchitektur analysieren und bestimmen  
   * B10.1 Die Architektur der Software bestimmen und die Entwicklung unter Berücksichtigung von Betrieb und Wartung planen und dokumentieren (Niveau: 3)
   * B10.2 Applikationen unter Beachtung übergeordneter Konzepte wie der ICT-Strategie, Standards etc. in die Softwarearchitektur integrieren (Niveau: 3)

 * B11 Applikationen entwickeln, Programme erstellen und testen
   * B11.1 Vorgaben für die Konzipierung eines Softwaresystems mit einer formalen Methode analysieren (Niveau: 3)
   * B11.2 Systemspezifikation interpretieren und die technische Umsetzung entwerfen (Niveau: 3)
   * B11.3 Spezifikation in einer geeigneten Programmiersprache umsetzen (Niveau: 3)
   * B11.7 Datenbanken aufgrund konzeptioneller Datenmodelle logisch abbilden und in Applikationen integrieren (Niveau: 3)

 * B12 System- und Netzwerkarchitektur bestimmen
   * B12.1 Die bestehende Systemarchitektur beurteilen und weiterentwickeln (Niveau: 3)
   * B12.2 Die bestehende Netzwerk-Architektur analysieren, Umsetzungsvarianten definieren und eine Soll-Architektur entwickeln (Niveau: 4)
   * B12.3 Bestehende ICT Konfigurationen analysieren, Umsetzungsvarianten für die Erweiterung definieren und Soll-Konfigurationen entwickeln (Niveau: 3)
   * B12.4 Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen (Niveau: 3)

 * B13 Konzepte und Services entwickeln
   * B13.1 Archiv-, Backup-, Restore- und Repair-Konzepte für die Software, Datenbestände und Datenbanken erarbeiten (Niveau: 3)
   * B13.2 Spezifische Testkonzepte erstellen und die Tests der relevanten Prüfobjekte planen (Niveau: 3)
   * B13.3 Services gemäss Pflichtenheft planen (Niveau: 3)
   * B13.4 Anforderungen aus dem Service-Management analysieren, entwickeln und integrieren (Niveau: 3)
   * B13.5 Service-Levels unter Berücksichtigung der Servicestrategie und Kundenvorgaben entwickeln (Niveau: 3)

 * B14 Konzepte und Services umsetzen
   * B14.1 Technische und organisatorische Massnahmen planen und für die Einführung von Software bzw. Releases ausarbeiten (Niveau: 3)
   * B14.2 Probleme und Fehler im operativen Betrieb überwachen, identifizieren, zuordnen, beheben oder falls erforderlich eskalieren (Niveau: 3)
   * B14.3 Die Kundenzufriedenheit bezüglich ICTDienstleistungen durch Messungen undUmfragen ermitteln (Niveau: 2)
   * B14.4 Risiken beim Betrieb von ICT-Systemen systematisch erheben und Massnahmen ableiten (Niveau: 3)
   * B14.5 ICT-Systeme und ICT-Dienstleistungen beschaffen (Niveau: 3)
   * B14.6 Standardverträge für ICT-Lizenzen einsetzen (Niveau: 3)
   * B14.7 Verrechnungsmodell operativ erstellen, umsetzen und ICT- Dienstleistungen budgetieren und verrechnen (Niveau: 2)

 * B15 Nachhaltiges Handeln
   * B15.1 Neue und bereits bestehende ICT-Systeme bezüglich Energieeffizienz und Umweltverträglichkeit evaluieren (Niveau: 2)
   * B15.2 Massnahmen zur Minimierung des Energieverbrauchs ergreifen (Niveau: 2)
   * B15.3 Den Einsatz von Material und natürlichen Ressourcen überwachen (Niveau: 2)
   * B15.4 Massnahmen zum Ersatz und zur Minimierung des Einsatzes von umweltschädigenden Materialien sowie zur Schliessung von Materialkreisläufen ergreifen (Niveau: 2)
   * B15.5 Tätigkeiten an den Kriterien einer ökonomischen, sozialen und ökologischen Nachhaltigkeit sowie ethischer Richtlinien ausrichten (Niveau: 2)
   * B15.6 Interaktionen gegenüber Dritten mit Respekt und Toleranz gestalten (Niveau: 2)
   * B15.7 Arbeitssicherheit und Gesundheitsschutz der Mitarbeitenden sowie Umweltschutz im eigenen Wirkungsbereich als Vorgesetzte/Vorgesetzter verantworten und gestalten (Niveau: 3)

 * C1 Cloud
   * C1.1 Analysiert die bestehende ICT-Umgebung und deren Struktur und beurteilt die Möglichkeit eine Private Cloud einzusetzen (Niveau: 2)
   * C1.2 Kann erste Entscheidungshilfen für sein Unternehmen liefern welche Cloud Services sinnvoll sind (Niveau: 3)
   * C1.3 Der Studierende ist in der Lage, Systeme zu evaluieren, deren Performance und die Eignung für den Cloud Anwendungszweck zu beurteilen (Niveau: 3)
   * C1.4 Der Studierende ist in der Lage ein Private Cloud System aufzusetzen und zu betreiben (Niveau: 2)
   * C1.5 Der Studierende ist in der Lage Services für die Cloud bereitzustellen, diese zu automatisieren und jederzeit zur reproduzieren (Niveau: 4)

 * C2 Cloud-native 
   * C2.1 Analysiert die bestehende ICT-Umgebung und deren Struktur und beurteilt die Möglichkeit Container einzusetzen (Niveau: 3)
   * C2.2 Kennt Container Umgebungen (Niveau: 3)
   * C2.3 Container Umgebungen aufsetzen und Bereitstellen (Niveau: 3)
   * C2.4 Container Cluster (Kubernetes) Umgebung aufsetzen und überwachen (Niveau: 3)
   * C2.5 Versteht die Begriffe CI/CD (Continuous Integration / Delivery) und kann diese den Cloud-Technologien zuordnen (Niveau: 3)
