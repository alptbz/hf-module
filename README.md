# TBZ Dipl. Informatiker/in HF Cloud-native Engineer - Modulübersicht

Script generiert Modulübersicht für den Lehrgang [Dipl. Informatiker/in HF Cloud-native Engineer der TBZ](https://tbz.ch/weiterbildung-tbz/it-services-engineer-hf-2/).

https://ch-tbz-wb.gitlab.io/general/hf-moduls

[Instruktionen](INSTRUCTIONS.md)

Der Rahmenlehrplan kann hier eingesehen werden: [SBFI - Rahmenlehrpläne - Dipl. Informatikerin HF](https://www.becc.admin.ch/becc/public/bvz/beruf/show/321)

# Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
