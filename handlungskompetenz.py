from typing import Self, List

from hfmodul import HfModul


class Handlungskompetenz:

    def __init__(self, id: str, description: str, niveau: int, is_main: bool = False):
        self.total_lernstunden: float = 0
        self.is_main = is_main
        self.id: str = id
        self.category = self.id[0]
        self.description: str = description
        self.niveau: int = niveau
        self.children: List[Self] = []
        self.moduls: List[HfModul] = []
        self.total_praesenzlektionen_vor_ort: float = 0
        self.total_selbststudium: float = 0
        self.total_praesenzlektionen_distance_learning: float = 0
        self.anteil_total_lernstunden: float = 0

    def link_module(self, hfmodul: HfModul):
        if hfmodul not in self.moduls:
            self.moduls.append(hfmodul)
        hfmodul.link_handlungskompetenz(self)

    def add_child(self, handlungskompetenz: Self):
        self.children.append(handlungskompetenz)

    def __str__(self):
        return f'{self.id} {self.description} ({self.niveau})'

    def __repr__(self):
        return self.__str__()

    def get_child(self, handlungskomp):
        child = [x for x in self.children if x.id == handlungskomp]
        return next(iter(child), None)

    def add_lernstunden(self, praesenzlektionen_vor_ort: float, selbststudium: float, praesenzlektionen_distance_learning: float):
        self.total_selbststudium += selbststudium
        self.total_praesenzlektionen_distance_learning += praesenzlektionen_distance_learning
        self.total_praesenzlektionen_vor_ort += praesenzlektionen_vor_ort
        self.total_lernstunden += (selbststudium
                                  + praesenzlektionen_distance_learning
                                  + praesenzlektionen_vor_ort)
